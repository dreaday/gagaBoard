package controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * ControllerServlet 클래스
 * action.properties의 설정된 클래스 이름을 읽어서
 * 해당 클래스의 객체를 commandMap 객체에 저장
 * => url 별로 클래스가 추가되어야 한다.*/

import action.ActionForward;
import action.AllAction;

/*
 * ControllerMethodServlet
 * method.properties의 설정된 내용은 메서드의 이름이다.
 * 메서드의 이름을 AllAction 클래스의 메서드를 호출 하도록 한다.
 * 
 * */
/**
 * Servlet implementation class ControllerMethod
 */
@WebServlet(urlPatterns = { "/aaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaa" }, initParams = {
		@WebInitParam(name = "properties", value = "D:/yyh/Myproject/GagaBoard/WebContent/WEB-INF/properties/method.properties") })

public class ControllerRefer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Properties pr = new Properties();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControllerRefer() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("1111111"); // 잘되나 실행즘. 뭐함 용훈.. 실행 언능 .
		/*
		String prop = config.getInitParameter("properties");
		FileInputStream f = null;
		BufferedInputStream b = null;
		try {
			// f : method.properties내용 읽기 위한 스트림
			b = new BufferedInputStream(new FileInputStream(prop));
			// method.properties 내용이
			// Properties 객체에 put 됨
			pr.load(b);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			try {
				if (f != null)
					f.close();
			} catch (Exception e2) {
			}
		}
		*/
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// uri : jspstudy2//hello.bo
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("euc-kr");
		ActionForward forward = null;
		String command = null;
		String code = request.getRequestURI();
		code = code.substring(code.lastIndexOf("/")+1);

// db select 추가해서 짤은 주소랑 저 code랑 조회하면 저건이 나올꺼 아니야 ~  아 ....무너지알거같아요 생성시 동시에 짧은주소까지 같이 만들어서 DB에 넣는다음 
		
		// 짧은주소로 접속했을떄  하면되겠네요  응 글도 맞찬가지야  /view/* 이런식. 하면 스퀸스랑 저거랑 연결시켜버리면되지. ㅇㅋ ?아  ..파라미터 지저분하게 핡덧도없이 ... 그냥 짧게 ..
		// 응 원리는 간단해 자바는 개편하니.. ㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋㅋ ㅋㅋ 일단ㅋ ㅋㅋㅋ 급한불부터 끄고 ㅋㅋ 해야겠네용 
		System.out.println("222222");
		System.out.println(code);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

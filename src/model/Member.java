package model;

import java.util.Date;

public class Member {
	private int seq;
	private int num;
	private String id;
	private String password;
	private String email;
	private String tel;
	private String profile_img;
	private String ip;
	private Date reg_date;
	private String reg_name;
	private String admin_YN;
	private String del_yn;

	public void setSeq(int seq) {
		this.num = seq;
	}

	public int getSeq() {
		return num;
	}

	public int getNum() {
		return num;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getProfile_img() {
		return profile_img;
	}

	public void setProfile_img(String profile_img) {
		this.profile_img = profile_img;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getReg_date() {
		return reg_date;
	}

	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}

	public String getReg_name() {
		return reg_name;
	}

	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}

	public String getAdmin_YN() {
		return admin_YN;
	}

	public void setAdmin_YN(String admin_YN) {
		this.admin_YN = admin_YN;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_YN) {
		this.del_yn = del_YN;
	}

	@Override
	public String toString() {
		return "Member [seq=" + seq + ", num=" + num + ", id=" + id + ", password=" + password + ", email=" + email
				+ ", tel=" + tel + ", profile_img=" + profile_img + ", ip=" + ip + ", reg_date=" + reg_date
				+ ", reg_name=" + reg_name + ", admin_YN=" + admin_YN + ", del_YN=" + del_yn + "]";
	}

}

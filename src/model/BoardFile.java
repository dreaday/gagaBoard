package model;

import java.util.Date;

public class BoardFile {
	private int seq;
	private int boardcontent_seq;
	private String file_name;
	private String file_path;
	private String file_size;
	private String ip;
	private Date reg_date;
	private String reg_name;
	private String del_yn;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getBoardcontent_seq() {
		return boardcontent_seq;
	}

	public void setBoardcontent_seq(int boardcontent_seq) {
		this.boardcontent_seq = boardcontent_seq;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_size() {
		return file_size;
	}

	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getReg_date() {
		return reg_date;
	}

	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}

	public String getReg_name() {
		return reg_name;
	}

	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}

	@Override
	public String toString() {
		return "BoardFile [seq=" + seq + ", boardcontent_seq=" + boardcontent_seq + ","
				+ ", file_name=" + file_name + ", file_path=" + file_path + ", file_size=" + file_size + ", ip=" + ip
				+ ", reg_date=" + reg_date + ", reg_name=" + reg_name + ", del_yn=" + del_yn + "]";
	}
	
	
}


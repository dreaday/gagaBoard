package util;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.oreilly.servlet.MultipartRequest;

import model.BoardDao;

public class Util {
	public static Map<String, Object> getParam(HttpServletRequest req) {
		Map<String, Object> rstMap = new HashMap<String, Object>();
		Enumeration<String> enu = req.getParameterNames();
		String key;
		String value;
		while (enu.hasMoreElements()) {
			key = (String) enu.nextElement();
			value = req.getParameter(key);
			if (value == null) {
				value = "";
			}
			System.out.println(key + " / " + value);
			rstMap.put(key, value);
		}

		return rstMap;
	}

	// 파일이 1개일 경우일 때
	public static Map<String, Object> getMultipartParam(HttpServletRequest req) throws IOException {
		Map<String, Object> rstMap = new HashMap<String, Object>();
		Date today = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		String today_folder = sf.format(today);
		String path = req.getServletContext().getRealPath("/") + today_folder + "/board/";
		File f = new File(path);
		String realFileName;
		boolean isDelete = false; // 파일 삭제
		int cnt = 1; // 파일 갯수 체크
		int i = -1;
		System.out.println("오늘자 폴더" + today_folder + ": " + f.isDirectory());
		if (!f.isDirectory()) {
			f.mkdirs();
		}
		MultipartRequest multi = new MultipartRequest(req, path, (10 * 1024 * 1024), "EUC-KR");
		Enumeration<String> enu = multi.getParameterNames();
		Enumeration<String> enu2 = multi.getFileNames();
		String key = null;
		String value = null;
		String[] str = { "jpg", "gif", "png", "JPG", "GIF", "PNG" };
		while (enu.hasMoreElements()) {
			key = (String) enu.nextElement();
			value = multi.getParameter(key);
			if (value == null) {
				value = "";
			}
			System.out.println(key + " / " + value);
			rstMap.put(key, value);
		}

		while (enu2.hasMoreElements()) {
			key = (String) enu2.nextElement();
			value = multi.getFilesystemName(key);
			if (value == null) {
				value = "";
				rstMap.put("boardimage", value);
			} else {
				value = value.trim();
				realFileName = Util.md5(value.substring(0, value.indexOf(".")) + System.currentTimeMillis())
						+ value.substring(value.indexOf("."), value.length());
				File oldFile = new File(path, value);
				File newFile = new File(path, realFileName);
				oldFile.renameTo(newFile);
				value = realFileName;
				System.out.println(key + "/ " + value);
				for (String s : str) {
					if (s.equalsIgnoreCase(value.substring(value.indexOf(".") + 1))) {
						rstMap.put("boardimage", today_folder+"/board/"+value.substring(0,value.lastIndexOf("."))+value.substring(value.indexOf(".")));
						isDelete = true;
						break;
					}
				}
				if (isDelete) {
					return rstMap;
				} else {
					newFile.delete();
				}
			}
		}
		return null;
	}

	// 파일이 다중일 경우일 때
	public static Map<String, Object> getMultiFileParam(HttpServletRequest req) throws IOException {
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		Map<String, Object> rstMap = new HashMap<String, Object>();
		Map<String, Object> fileMap = new HashMap<String, Object>();
		Date today = new Date();
		File newFile = null;
		List file_name = new ArrayList<>();
		List file_size = new ArrayList<>();
		int cnt = 0;
		int i = -1;
		String today_folder = sf.format(today);
		String realFileName;
		String[] str = { "jpg", "gif", "png", "JPG", "GIF", "PNG" };
		String key = null;
		String value = null;
		String code = req.getParameter("code");
		long size;
		// 파일클래스 경로 설정
		String path = today_folder + "/" + code;
		String abspath = req.getServletContext().getRealPath("/") + today_folder + "/" + code;
		File f = new File(abspath);
		boolean isDelete = false;
		BoardDao bdao = new BoardDao();
		System.out.println("오늘자 폴더" + today_folder + ": " + f.isDirectory());
		if (!f.isDirectory()) {
			f.mkdirs();
		}
		MultipartRequest multi = new MultipartRequest(req, abspath, (10 * 1024 * 1024), "EUC-KR");

		Enumeration<String> enu = multi.getParameterNames();
		Enumeration<String> enu2 = multi.getFileNames();
		while (enu.hasMoreElements()) {
			key = (String) enu.nextElement();
			value = multi.getParameter(key);
			if (value == null) {
				value = "";
			}
			System.out.println("파라미터 : " + key + " / " + value);
			rstMap.put(key, value);
		}

		while (enu2.hasMoreElements()) {
			key = (String) enu2.nextElement();
			value = multi.getFilesystemName(key);
			if (value == null) {
				value = "";
			} else {
				value = value.trim();
				// 변경할 파일 이름
				realFileName = Util.md5(value.substring(0, value.indexOf(".")) + System.currentTimeMillis())
						+ value.substring(value.lastIndexOf("."));
				// 이미 생성된 기존 파일이름
				File oldFile = new File(abspath, value);
				// 새로운 파일 이름
				newFile = new File(abspath, realFileName);
				oldFile.renameTo(newFile);
				size = newFile.length();

				value = realFileName;
				System.out.println("파일 : " + key + " / " + value);
				for (String s : str) {
					// 확장자 같은 게 있으면 true

					System.out.println(s.equalsIgnoreCase(value.substring(value.indexOf(".") + 1)));
					if (s.equalsIgnoreCase(value.substring(value.indexOf(".") + 1))) {
						file_name.add(realFileName);
						file_size.add(size);
						isDelete = false;
						break;
					} else {
						isDelete = true;
					}
				}
				if (isDelete) {
					newFile.delete();
				}

			}
		}

		rstMap.put("file_path", path);
		rstMap.put("file_name", file_name);
		rstMap.put("file_size", file_size);
		return rstMap;
	}

	public static String md5(String s) {
		String rstStr = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(s.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1) + s);
			}
			rstStr = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			rstStr = null;
		}
		return rstStr.substring(0, 30);
	}

	// 액션에 /board/* 해줘.하나 만들어줘 그렇게 안되 ? ??
	// 용훈. 액션 오디야 처음 타는곳이. ?? s넵 ??
	// 저파일 하나 더 만들어도되?
	// 넵 컨트롤러 또만들어두되용 복사해줘.
	public static String c36dec(int dec) {
		String key = "0123456789abcdefghijklmnopqrstuvwxyz";
		int tmp1 = (int) (dec / 36);
		int tmp2 = dec % 36;
		String c36 = String.valueOf(key.charAt(tmp2));
		if (tmp1 > 0)
			c36 = c36dec(tmp1) + c36;
		return c36;
	}

	public static void main(String[] args) {
		System.out.println(c36dec(10000000));
		// 22번 스퀸스에 m넣어줘 10000000 1000만건이 신청되도 저런식으로 하면 겨우 코드 5자리야.
		// 이해됨 ? 저걸 db에 넣고. 예들어줄게 램덤으로 만약 들어가게 저거 넣어줘 아무게시판에 // 5yc1s
		// 이제 조회를 해주면되지.
		// 조회해서 매치되면
	}
}

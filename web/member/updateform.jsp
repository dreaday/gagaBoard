<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>회원가입 Join Form</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var id = $("#id");
						var password1 = $("#password1");
						var password2 = $("#password2");
						var name = $("#reg_name");
						var email = "";
						var tel = $("#telselect option:selected").text();
						var flag = true;
						$("#emailcheck")
								.click(
										function() {
											if ($("#emailcheck").is(":checked")) {
												document
														.getElementById("emailselect").style.display = "none";
												document
														.getElementById("emailtext").style.display = "block";
											} else {
												document
														.getElementById("emailselect").style.display = "block";
												document
														.getElementById("emailtext").style.display = "none";
											}
										})
						$("#submit")
								.click(
										function() {
											if (!id.val()) {
												alert('아이디부터 입력하세요.');
												id.focus();
												return false;
											}
											if (!password1.val()) {
												alert('비밀번호 1번을 입력 해 주세요.')
												password1.focus()
												return false;
											}
											if (!password2.val()) {
												alert('비밀번호 2번을 입력 해 주세요.')
												password2.focus()
												return false;
											}
											if (password1.val() != password2
													.val()) {
												alert("1번과 2번의 비밀번호가 다릅니다.");

												return false;
											}
											if (!name.val()) {
												alert('이름을 입력 해 주세요.');
												name.focus();
												return false;
											}

											email = $("#emailid");
											if (!email.val()) {
												alert('이메일주소를 입력 해 주세요');
												email.focus();
												return false;
											} else {
												email = $("#emailid").val();
												if ($("#emailcheck").is(
														":checked")) {
													email += '@'
															+ $("#emaildomain2")
																	.val();
												} else {
													email += '@'
															+ $(
																	"#emaildomain option:selected")
																	.text();
												}
												// 정규식 - 이메일 유효성 검사
												var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
												// 정규식 -전화번호 유효성 검사/* 
												var regTel = /^((01[1|6|7|8|9])[1-9]+[0-9]{6,7})|(010[1-9][0-9]{7})$/;
												
												if (!regEmail.test(email)) {
													alert('이메일 주소가 유효하지 않습니다');
													email = $("#emailid");
													email.focus();
													return false;
												}
												return true;
											}
											return true;
										})

					})
</script>
</head>
<body>
	<c:if test="${!empty login }">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="page-header">
						<h1>
							회원정보 수정 <small>GagaBoard UpdateForm</small>
						</h1>
					</div>
					<div style="margin-left: 80px">
						<form action="./member/update.ga" method="post">
							<div class="form-group">
								<label for="id">아이디</label> <input type="text"
									class="form-control " id="id" name="id"
									value="<c:out value='${member.id}'/>" readonly>
							</div>
							<div class="form-group">
								<label for="password1"><strong>비밀번호1</strong></label> <input
									type="password" class="form-control" id="password1"
									name="password1">
							</div>

							<div class="form-group">
								<label for="password2"><strong>비밀번호2</strong></label> <input
									type="password" class="form-control" id="password2"
									name="password2">
							</div>
							<div class="form-group">
								<label for="nickname"><strong>이름</strong></label> <input
									type="text" class="form-control" id="reg_name" name="reg_name"
									value="<c:out value='${member.reg_name}'/>" readonly>
							</div>
							<div class="form-group">
								<label for="email"><strong>이메일</strong></label>
								<div class="row">
									<div class="col-lg-4">
										<input type="text" class="form-control" id="emailid"
											name="emailid" value="<c:out value='${emailid}'/>"readonly>
									</div>
									<div class="col-lg-1">
										<strong>@</strong>
									</div>
									<div id="emailselect" class="col-lg-5" style="display: none;">
										<select id="emaildomain" name="emaildomain"
											class="selectpicker form-control">
											<option>gmail.com</option>
											<option>naver.com</option>
											<option>daum.net</option>
											<option>nate.com</option>
										</select>
									</div>
									<div id="emailtext" class="col-lg-5">
										<input type="text" class="form-control" id="emaildomain2"
											name="emaildomain" value="<c:out value='${emaildomain}'/>"
											style="display: block;" readonly>
									</div>
									<div class="col-lg-2">
										<label class="checkbox-inline"><input type="checkbox"
											id="emailcheck" value="">변경하려면 클릭</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="tel"><strong>연락처</strong></label> <input
									type="hidden" class="form-control" id="tel" name="tel"
									value="<c:out value='${member.tel}'/>">
							</div>
							<div class="form-group">
								<label for="profile_img"><strong>프로필사진</strong></label> <input
									type="hidden" class="form-control" id="profile_img"
									name="profile_img"
									value="<c:out value='${member.profile_img}'/>">
							</div>

							<div class="form-group text-center">
								<button type="submit" id="submit" class="btn btn-info">
									회원수정<i class="fa fa-check spaceLeft"></i>
								</button>
								<button href="../main.jsp" class="btn btn-warning">
									가입취소<i class="fa fa-times spaceLeft"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- row 끝 -->
		<!-- container 끝 -->
	</c:if>
	<c:if test="${empty login or admin }">
		<script type="text/javascript">
			alert("로그인 후 접속하시기 바랍니다. 올바르게 접속 하지 않았습니다.");
			location.href = "../main.jsp";
		</script>

	</c:if>
</body>
</html>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.lang.model.util.SimpleAnnotationValueVisitor6"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<link rel="stylesheet"
	href="./sc/bootstrap-3.3.2/dist/css/bootstrap.css">
</head>
<%
	SimpleDateFormat sf = new SimpleDateFormat("yyyy-dd-MM-hh:mm:ss");
	Date today = new Date();
	String date = sf.format(today);
%>
<body>
	<div style="width: 60%; margin-left: 20px; margin-top: 20px;">
		<table class="table table-striped table-bordered table-hover">
			<tr align="center">

				<td>번호</td>
				<td>작성자</td>
				<td>직급</td>
				<td>제목</td>
				<td>작성일</td>
				<td>수정일</td>
				<td>조회수</td>
			</tr>
			<tr align="center">
				<td>1</td>
				<td>홍길동</td>
				<td>사장</td>
				<td>가가보드 신청 관련 서류 제출</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>15</td>
			</tr>
			<tr align="center">
				<td>2</td>
				<td>임꺽정</td>
				<td>부장</td>
				<td>가가보드 신청 제출 결제건</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>10</td>
			</tr>
			<tr align="center">
				<td>3</td>
				<td>성춘향</td>
				<td>차장</td>
				<td>가가보드 신청건 (기획)</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>5</td>
			</tr>
			<tr align="center">
				<td>4</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트1</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>5</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트2</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>6</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트3</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>7</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트4</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>8</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트5</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>9</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트6</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>

		</table>
	</div>
	<div style="width: 60%; margin-left: 20px; margin-top: 20px;">
		<table class="table table-striped table-bordered table-hover">
			<tr align="center">

				<td>번호</td>
				<td>작성자</td>
				<td>직급</td>
				<td>제목</td>
				<td>작성일</td>
				<td>수정일</td>
				<td>조회수</td>
			</tr>
			<tr align="center">
				<td>1</td>
				<td>홍길동</td>
				<td>사장</td>
				<td>가가보드 신청 관련 서류 제출</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>15</td>
			</tr>
			<tr align="center">
				<td>2</td>
				<td>임꺽정</td>
				<td>부장</td>
				<td>가가보드 신청 제출 결제건</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>10</td>
			</tr>
			<tr align="center">
				<td>3</td>
				<td>성춘향</td>
				<td>차장</td>
				<td>가가보드 신청건 (기획)</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>5</td>
			</tr>
			<tr align="center">
				<td>4</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트1</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>5</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트2</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>6</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트3</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>7</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트4</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>8</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트5</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>9</td>
				<td>조조</td>
				<td>과장</td>
				<td>게시판 테스트6</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
		</table>
	</div>
	<div
		style="width: 60%; height: 800px; margin-left: 20px; margin-top: 20px;">
		<table class="table table-striped table-bordered table-hover">
			<tr align="center">

				<td>번호</td>
				<td>작성자</td>
				<td>직급</td>
				<td>제목</td>
				<td>작성일</td>
				<td>수정일</td>
				<td>조회수</td>
			</tr>
			<tr align="center">
				<td>1</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>2</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>3</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>4</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>5</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>6</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>7</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>8</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>9</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
		</table>
	</div>

	<div
		style="width: 60%; height: 800px; margin-left: 20px; margin-top: 20px;">
		<table class="table table-striped table-bordered table-hover">
			<tr align="center">

				<td>번호</td>
				<td>작성자</td>
				<td>직급</td>
				<td>제목</td>
				<td>작성일</td>
				<td>수정일</td>
				<td>조회수</td>
			</tr>
			<tr align="center">
				<td>1</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>2</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>3</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>4</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>5</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>6</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>7</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>8</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>9</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
		</table>
	</div>



	<div
		style="width: 60%; height: 800px; margin-left: 20px; margin-top: 20px;">
		<table class="table table-striped table-bordered table-hover">
			<tr align="center">
				<td>번호</td>
				<td>작성자</td>
				<td>직급</td>
				<td>제목</td>
				<td>작성일</td>
				<td>수정일</td>
				<td>조회수</td>
			</tr>
			<tr align="center">
				<td>1</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>2</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>3</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>4</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>5</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>6</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>7</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>20</td>
			</tr>
			<tr align="center">
				<td>8</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
			<tr align="center">
				<td>9</td>
				<td>관리자</td>
				<td>관리자</td>
				<td>GoXXX님의 게시판 신청</td>
				<td><%=date%></td>
				<td><%=date%></td>
				<td>0</td>
			</tr>
		</table>
	</div>
	<article class="container">
	<div class="page-header">
		<h1>
			나의 가가보드 관리 <small>My GagaBoard PlatForm</small>
		</h1>
	</div>
	<div
		style="width: 80%; height: 800px; margin-left: 20px; margin-top: 20px; margin: auto;">
		<table class="table table-bordered table-hover">
			<tr class="warning" align="center">

				<td>번호</td>
				<td>게시판이름</td>
				<td>담당자</td>
				<td>참여자</td>
				<td>생성일</td>
				<td>만료일</td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>2</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>3</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>4</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>5</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>

		</table>
	</div>
	11
	<div class="page-header">
		<h1>
			문의 게시판 <small>GagaBoard PlatForm Q & A</small>
		</h1>
	</div>
	<div
		style="width: 80%; height: 800px; margin-left: 20px; margin-top: 20px; margin: auto;">
		<table class="table table-bordered table-hover">
			<tr class="warning" align="center">

				<td>번호</td>
				<td>게시판이름</td>
				<td>담당자</td>
				<td>참여자</td>
				<td>생성일</td>
				<td>만료일</td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>접속이 안되요!!</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>2</td>
				<td>접속하게해주세요.</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>3</td>
				<td>5개에서 더 증가 할 수 있나요 ?</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>4</td>
				<td>추가로 사용하고싶습니다.</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
			<tr align="center">

				<td>5</td>
				<td>L사 영업부</td>
				<td>홍길동</td>
				<td>5명</td>
				<td><%=date%></td>
				<td><%=date%></td>
			</tr>
		</table>
	</div>
	<div class="page-header">
		<h1>
			L사 권한관리 <small>Lcompany Permission</small>
		</h1>
	</div>
	<div
		style="width: 30%; height: 800px; margin-left: 20px; margin-top: 20px;">
		<table class="table table-bordered table-hover">
			<tr class="warning" align="center">

				<td>번호</td>
				<td>이름</td>
				<td>권한</td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>홍길동</td>
				<td><select><option>관리자</option>
						<option>회원</option></select></td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>홍길동</td>
				<td><select><option>관리자</option>
						<option>회원</option></select></td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>홍길동</td>
				<td><select><option>관리자</option>
						<option>회원</option></select></td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>홍길동</td>
				<td><select><option>관리자</option>
						<option>회원</option></select></td>
			</tr>
			<tr align="center">

				<td>1</td>
				<td>홍길동</td>
				<td><select><option>관리자</option>
						<option>회원</option></select></td>
			</tr>
		</table>
	</div>

	<div class="page-header">
		<h1>
			글작성<small> LCompany Write PlatForm</small>
		</h1>
	</div>
	<div
		style="width: 80%; height: 800px; margin-left: 20px; margin-top: 20px; margin: auto;">
		<table class="table table-bordered table-hover">
			<tr class="warning" align="center">
				<td><b>게시판이름</b></td>
				<td><input type="text" id="Write" size="100%"></td>
			</tr>
			<tr class="warning" align="center">
				<td><b>제목</b></td>
				<td><input type="text" id="Subject" size="100%"></td>
			</tr>
			<tr class="warning" align="center">
				<td style=""><b>내용</b></td>
				<td><textarea id="ContentForm" cols="100" rows="15" ></textarea></td>
			</tr>
			<tr class="warning" >
				<td align="center"><div style="float: left;"><b>파일</b></div><button id="plus">+</button><button id="minus">-</button></td>
				<td align="left"><input type="file" id="Write"></td>
			</tr>
		</table>
	</div>
		<div class="page-header">
		<h1>
			글수정<small> LCompany Modify PlatForm</small>
		</h1>
	</div>
		<div
		style="width: 80%; height: 800px; margin-left: 20px; margin-top: 20px; margin: auto;">
		<table class="table table-bordered table-hover">
			<tr class="warning" align="center">
				<td><b>게시판이름</b></td>
				<td><input type="text" id="Write" size="100%" value="L사게시판"></td>
			</tr>
			<tr class="warning" align="center">
				<td><b>제목</b></td>
				<td><input type="text" id="Subject" size="100%" value="밥주세요 부장님"></td>
			</tr>
			<tr class="warning" align="center">
				<td style=""><b>내용</b></td>
				<td><textarea id="ContentForm" cols="100" rows="15" >배고파요 밥주세요</textarea></td>
			</tr>
			<tr class="warning" >
				<td align="center"><div style="float: left;"><b>파일</b></div><button id="plus">+</button><button id="minus">-</button></td>
				<td align="left"><input type="file" id="Write"></td>
			</tr>
		</table>
	</div>
	</article>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.3.js"></script>
<script src="./sc/bootstrap-3.3.2/dist/js/bootstrap.js"></script>

</body>
</html>
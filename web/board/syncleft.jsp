<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="row " style="background-color: #fafafa; height: 20%; margin: 0px; padding: 0px;font-size:15px; font-weight: bold;">
	<div class="col-lg-2">번호</div>
	<div class="col-lg-4">아이피</div>
	<div class="col-lg-2">제목</div>
	<div class="col-lg-3">등록일자</div>
</div>

<c:forEach var="s" items="${list}">
	<fmt:formatDate var="t" value="${s.reg_date}"
		pattern="yyyy-MM-dd HH:mm:ss" />
	<div class="row alert "
		style="background-color: #fafafa; overflow: hidden; margin: 0px; padding: 0px; height: 20%; font-weight: bold; color: rgb(28, 188, 157)">
		<div class="col-lg-2">${s.member_seq}</div>
		<div class="col-lg-4">${s.ip}</div>
		<div class="col-lg-2">${s.reg_name}</div>
		<div class="col-lg-3">${t}</div>
	</div>
</c:forEach>


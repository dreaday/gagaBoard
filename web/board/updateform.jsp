<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>회원가입 Join Form</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/boots	trap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-filestyle.js"></script>

<script type="text/javascript">
	
</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="page-header">
					<h1>
						게시글 수정 <small>Board Content UpdateForm</small>
					</h1>
				</div>
				<form action="update.ga" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="hidden" class="form-control " name="seq"
							value="<c:out value='${param.seq}'/>">
					</div>

					<div class="form-group">
						<input type="hidden" class="form-control " name="code"
							value="<c:out value='${param.code}'/>">
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control " name="id"
							value="<c:out value='${param.id}'/>">
					</div>

					<div class="form-group">
						<label for="subject"><strong>제목</strong></label> <input
							type="text" class="form-control" name="subject"
							value="<c:out value='${board.subject}'/>">
					</div>
					<div class="form-group">
						<label for="content">내용</label>
						<textarea class="form-control" rows="3" name="content"><c:out
								value='${board.content}' /></textarea>
					</div>
					<div class="form-group">
						<label for="비밀번호"><strong>비밀번호</strong></label> <input
							type="password" class="form-control" name="password">
					</div>
					<div class="form-group">
						<div id="fileform" class="col-lg-12">
							<label for="file">파일 업로드</label> <input type="button"
								class="btn btn-primary"
								onclick="javascript:plus(); return false;" value="+"> <input
								type="button" class="btn btn-danger"
								onclick="javascript:minus(); return false;" value="-"> <br>
							<br>
							<c:if test="${!empty list }">
								<c:forEach var="a" items="${list}" varStatus="stat">
									<input type="file" class="filestyle" name="file${stat.index}"
										data-buttonName="btn-primary"
										value="./<c:out value='${a.file_path }'/>/<c:out value='${a.file_name }'/>">

								</c:forEach>
							</c:if>
							<c:if test="${empty list}">
								<input type="file" class="filestyle" name="file"
									data-buttonName="btn-primary">
							</c:if>

						</div>
					</div>
					<br>
					<div class="form-group center-block" style="text-align: center">
						<!-- Indicates a successful or positive action -->
						<br> <br> <br> <br>
						<button type="submit" class="btn btn-success">글작성</button>
						<button type="button" class="btn btn-danger"
							onclick="javascript:history.go(0)">취소</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- row 끝 -->
	<!-- container 끝 -->

	<c:if test="${empty login or admin }">
		<script type="text/javascript">
			alert("로그인 후 접속하시기 바랍니다. 올바르게 접속 하지 않았습니다.");
			location.href = "../main.jsp";
		</script>

	</c:if>
</body>
</html>
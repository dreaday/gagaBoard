<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title></title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>

<script src="../js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#popmo').click(function() {
			$.ajax({
				type : 'POST',
				enctype : 'multipart/form-data',
				url : 'modifyform.ga',
				data : {
					code : '<c:out value="${param.code}"/>',
					id : '<c:out value="${login}"/>'
				},
				success : function(data) {
					$('#modify').modal();
					$('#modalmodify').html(data);
				}
			})
		})
		$('#peradd').click(function() {
			$('#permissionadd').modal({
				remote : 'permissionadd.jsp'
			})
		})
		$('#per').click(function() {
			$.ajax({
				type : 'POST',
				url : 'permission.ga',
				data : {
					code : '<c:out value="${param.code}"/>',
					id : '<c:out value="${login}"/>'
				},
				success : function(data) {
					$('#permissions').modal();
					$('#permbody').html(data);
				}
			})
		})
	})
</script>
<style type="text/css">
.test {
	border: 1px solid;
	height: 100%;
}

.test2 {
	width: 100%;
	height: 100%;
}
</style>
<script type="text/javascript">
	function listSubmit(pageNum) {
		var find = encodeURI(encodeURIComponent(document.sf.find.value));
		location.href = "list.ga?code=" + "<c:out value='${param.code}'/>"
				+ "&id=" + "<c:out value='${login}'/>" + "&pageNum=" + pageNum
				+ "&column=" + "${param.column}" + "&find=" + find;
	}

	function info(infoNum) {
		location.href = "view.ga?code=" + "<c:out value='${param.code}'/>"
				+ "&id=" + "<c:out value='${login}'/>" + "&seq=" + infoNum;
	}
</script>

</head>
<body>
	<c:if test="${!empty login and login != ''}">
		<div class="container-fluid">
			<div class="center-block" style="width: 70%; padding: 0px">
				<div class="row" style="margin: 0px">
					<form action="list.ga" method="post" name="sf">
						<div class="col-lg-4 " style="width: 30%; height: 7%;"
							style="margin: 0px; padding: 0px">
							<select id="column" name="column" class="selectpicker "
								data-style="btn-" data-width="100%">
								<option style="font-style: white;" value="">선택하세요</option>
								<option value="subject">제목</option>
								<option value="ip">아이피</option>
								<option value="content">내용</option>
								<option value="reg_name">작성자</option>
								<option value="reg_date">작성시간</option>
								<option value="modify_date">수정시간</option>
							</select>
						</div>
						<div class="formgroup">
							<input type="hidden" class="form-control input-lg"
								style="width: 100%" height="100%" name="code"
								value="<c:out value='${param.code}'/>">
						</div>
						<div class="formgroup">
							<input type="hidden" class="form-control input-lg"
								style="width: 100%" height="100%" name="id"
								value="<c:out value='${login}'/>">
						</div>
						<div class="col-lg-5" style="float: left; width: 50%;">
							<input type="text" class="form-control input-lg"
								style="width: 100%" height="100%" name="find"
								value="<c:out value='${find}'/>">
						</div>
						<div class="col-lg-1 btn" style="width: 20%; float: left">
							<button type="submit" class="btn btn-primary">검색</button>
						</div>
						<div class="col-lg-1 btn" style="width: 20%; float: left">
							<button onclick="javascript:location.href='../main.jsp'"
								class="btn btn-primary">메인으로</button>
							<button onclick="javascript:location.href='../main.jsp'"
								class="btn btn-primary">나의 가가리스트</button>

						</div>

						<script type="text/javascript">
							document.sf.column.value = "<c:out value='${param.column}'/>";
						</script>
					</form>
				</div>
			</div>
			<div class="row center-block">
				<p>
				<div class="table-responsive ">
					<table class="table table-bordered ">
						<thead class="thead-inverse" id="thread">
							<tr class="alert alert-warning">
								<th style="width: 6%; text-align: center;"><strong>#</strong></th>
								<th style="width: 50%; text-align: center;"><strong>제목</strong></th>
								<th style="width: 7.5%; text-align: center;"><strong>IP</strong></th>
								<th style="width: 7.5%; text-align: center;"><strong>조회수</strong></th>
								<th style="width: 10%; text-align: center;"><strong>작성자</strong></th>
								<th style="width: 10%; text-align: center;"><strong>작성시간</strong></th>
								<th style="width: 10%; text-align: center;"><strong>수정시간</strong></th>

							</tr>
						</thead>
						<c:if test="${listcount==0 }">
							<td align="center" colspan="12">등록된 글이 없습니다.</td>
						</c:if>
						<c:if test="${listcount>0 }">
							<tbody>
								<c:forEach var="bbslist" items="${list}" varStatus="stat">
									<tr style="text-align: center;">
										<th style="text-align: center" scope="row"><c:out
												value="${boardNum}" /></th>
										<c:set var="boardNum" value="${boardNum-1 }" />

										<td><c:if test="${!empty bbslist.subject}">
												<a href="javascript:info('<c:out value='${bbslist.seq}'/>')">

													<%--답변글 인경우 들여쓰기및 답변글 표시 --%> <c:forEach begin="1"
														end="${bbslist.reflevel }">
									                  &nbsp;&nbsp;&nbsp;
									                  </c:forEach> <c:if test="${bbslist.reflevel>0 }">
														<img style="width: 20px; height: 20px;"
															src="../icon/deps.png">
													</c:if> <c:out value="${bbslist.subject}" />
												</a>
											</c:if> <c:if test="${empty bbslist.subject}">
												<a href="javascript:info('<c:out value='${bbslist.seq}'/>')">
													제목이 비어있습니다. </a>
											</c:if></td>

										<td><c:out value="${bbslist.ip}" /></td>
										<td><c:out value="${bbslist.readcnt}" /></td>
										<td><c:out value="${bbslist.reg_name}" /></td>
										<fmt:formatDate var="s" value="${bbslist.reg_date}"
											pattern="yyyy-MM-dd" />
										<td><c:out value="${s}" /></td>
										<fmt:formatDate var="d" value="${bbslist.modify_date}"
											pattern="yyyy-MM-dd" />
										<td><c:out value="${d}" /></td>
									</tr>
								</c:forEach>
							</tbody>
							<tr align="center">
								<td colspan="11">
									<ul class="pagination pagination-lg">

										<c:if test="${pageNum <=1}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:if test="${pageNum >1}">
											<li><a
												href="javascript:listSubmit('<c:out value='${pageNum -1}'/>');">
													<span class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:forEach var="a" begin="${startpage }" end="${endpage }">

											<c:if test="${a==pageNum }">
												<li class="active"><a href="#"> <c:out value='${a}' />
												</a></li>
											</c:if>

											<c:if test="${a!=pageNum }">
												<li class=""><a
													href="javascript:listSubmit('<c:out value='${a}'/>');">${a}</a>
												</li>
											</c:if>

										</c:forEach>

										<c:if test="${pageNum>=maxpage}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-right"></span>
											</a></li>
										</c:if>

										<c:if test="${pageNum<maxpage }">
											<li class=""><a
												href="javascript:listSubmit('<c:out value='${pageNum +1}'/>');">
													<span class="glyphicon glyphicon-chevron-right"> </span>
											</a></li>

										</c:if>
									</ul>
								</td>
							</tr>
						</c:if>
					</table>

				</div>
			</div>
		</div>
		<div class="row" style="margin: 0 auto; text-align: center;">
			<!-- Trigger the modal with a button -->
			<button type="button" class="btn btn-info btn-lg"
				onclick="javascript:location.href='writeform.ga?code=<c:out value="${param.code}"/>&id=<c:out value="${login}"/>'"
				style="margin: auto">글쓰기</button>

			<button type="button" id="per" class="btn btn-info btn-lg"
				style="margin: 0 0 0 5%">권한관리</button>

			<button type="button" id="peradd" class="btn btn-info btn-lg"
				style="margin: 0 0 0 5%">권한관리 추가</button>

			<button type="button" id="popmo" class="btn btn-success btn-lg "
				style="margin: 0 0 0 5%">수정</button>


			<button type="button" class="btn btn-info btn-lg" data-toggle="modal"
				data-target="#delete" style="margin: 0 0 0 5%">삭제</button>
			<!-- 모달 권한관리 -->
			<div id="permissions" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg" style="height: 200px;">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">게시판 권한 목록</h4>
						</div>
						<div id="permbody" class="modal-body">
							<p>권한관리</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>

			<!-- 권한추가 -->
			<div id="permissionadd" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">
								<strong>게시판 권한 추가</strong>
							</h3>
						</div>
						<div id="permissionaddbody" class="modal-body"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			<!--  권한추가 모달-->
			<!-- 삭제 -->
			<div id="delete" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">
								<strong>게시판 삭제 요청</strong>
							</h3>
						</div>
						<div class="modal-body">
							<p>
							<h3>정말 삭제 하시겠습니까?</h3>
							<p>
								<button type="button" class="btn btn-success"
									style="width: 100px;"
									onclick="javascript:location.href='delete.ga?code=<c:out value="${param.code}"/>&id=<c:out value="${login}"/>'">확인</button>
								<button type="button" class="btn btn-danger"
									style="width: 100px;" onclick="javascript:history.go(0)">취소</button>
							</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</div>
					</div>

				</div>
			</div>
			<!--  삭제 모달-->
			<!--  수정모달 시작 -->
			<div id="modify" class="modal fade" role="dialog">
				<div class="modal-dialog" style="width: 45%; margin: auto;">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">
								<strong><c:out value="${board.subject}" /> 글 수정 화면 (<c:out
										value="${login}" />)</strong>
							</h3>
						</div>
						<div id="modalmodify" class="modal-body" style="width: 200px">

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>


	</c:if>
	<c:if test="${empty login or login == ''}">
		<script type="text/javascript">
			alert("로그인 후 이용 바랍니다.")
			location.href = '../main.jsp';
		</script>
	</c:if>
</body>
</html>
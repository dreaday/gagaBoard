<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="row "
	style="background-color: #fafafa; height: 20%; margin: 0px; padding: 0px; font-size: 15px; font-weight: bold;">
	<div class="col-lg-2">게시판이름</div>
	<div class="col-lg-1">검사</div>
	<div class="col-lg-2">등록자</div>
	<div class="col-lg-3">등록일자</div>
	<div class="col-lg-4">사유</div>
	
</div>

<c:forEach var="s" items="${list}">
	<c:if test="${s.boardcheck eq 'Y'}">
		<fmt:formatDate var="t" value="${s.reg_date}"
			pattern="yyyy-MM-dd HH:mm:ss" />
		<div class="row alert "
			style="background-color: #fafafa; overflow: hidden; margin: 0px; padding: 0px; height: 20%; font-weight: bold; color: #e74c3c">
			<div class="col-lg-2">${s.boardname}</div>
			<div class="col-lg-1">${s.boardcheck}</div>
			<div class="col-lg-2">${s.reg_name}</div>
			<div class="col-lg-3">${t}</div>
			<div class="col-lg-4"> &lt;사유 : ${s.checkmsg} &gt;</div>
		</div>
	</c:if>
	<c:if test="${s.boardcheck eq 'N'}">
		<fmt:formatDate var="t" value="${s.reg_date}"
			pattern="yyyy-MM-dd HH:mm:ss" />
		<div class="row alert "
			style="background-color: #fafafa; overflow: hidden; margin: 0px; padding: 0px; height: 20%; font-weight: bold; color: #3498db">
			<div class="col-lg-2">${s.boardname}</div>
			<div class="col-lg-1">${s.boardcheck}</div>
			<div class="col-lg-2">${s.reg_name}</div>
			<div class="col-lg-3">${t}</div>
			<div class="col-lg-4">&lt; ${s.checkmsg} &gt; </div>
		</div>
	</c:if>
</c:forEach>


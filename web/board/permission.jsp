<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title></title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>

<script src="../js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	
</script>

</head>
<body>
	<c:if test="${!empty login and login != ''}">
		<table class="table table-responsive table-striped alert">
			<tr>
				<th>회원 이름</th>
				<th>관리자여부</th>
				<th>게시판 코드</th>
				<th>회원 번호</th>
			</tr>
			<c:forEach var="pem" items="${list}">
				<tr>
					<td><c:out value="${pem.reg_name }" /></td>
					<td><c:out value="${pem.admin_yn}" /></td>
					<td><c:out value="${pem.boardlist_code}" /></td>
					<td><c:out value="${pem.member_Seq}" /></td>
					
				</tr>
			</c:forEach>
		</table>
	</c:if>
	<c:if test="${empty login or login == ''}">
		<script type="text/javascript">
			alert("로그인 후 이용 바랍니다.")
			location.href = '../main.jsp';
		</script>
	</c:if>
</body>
</html>